
## Docker and Docker-Compose uses .env filen:
COMPOSE_CONVERT_WINDOWS_PATHS=1
    - makes it possible to share Sockets on Windows    
COMPOSE_PROJECT_NAME=learn_ansible_using_azure_e911e85c
    - overrides the default name of the compose project name so that the name of the network becomes unique


## When cloning this repository:
- create new partial GUID id => i.e. *e911e85c*-4577-4A58-8FDC-5276D6794D9E
- create new COMPOSE_PROJECT_NAME + GUID id 
- Rename "learn_ansible_using_azure_e911e85c" to something new.
- Rename "learn_ansible_using_azure_e911e85c" to something new.
- Rename "dockersecrets_e911e85c" to something new.
- Run "Dockerfile.build.bat" to build the root Container for this project
- Run "docker-compose.up.bat" 


