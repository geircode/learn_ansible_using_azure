#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

az group delete --name ansible001-rg --no-wait
az group delete --name ansible002-rg --no-wait
