import pprint, sys, os
pp = pprint.PrettyPrinter(indent=4)
import argparse
import json

parser = argparse.ArgumentParser(description='Get interpolated Parameterfile')
parser.add_argument('--vmname', default="ansible001")
args = parser.parse_args()

class AzureService:

    # def __init__(self):

    def getParametersFileContent(self,):
        relativeFilename = os.path.join(os.path.dirname(__file__), "parameters.json")
        with open(relativeFilename, 'r') as the_file: 
            filecontent = the_file.read()
        return filecontent

    def getInterpolatedParameters(self, vmName):
        filecontent = self.getParametersFileContent()
        return filecontent.replace("vmName", vmName)


if __name__ == '__main__':    
    parameters = AzureService().getInterpolatedParameters(vmName=args.vmname)
    loaded = json.loads(parameters)
    print(json.dumps(loaded))
    # print(parameters)
    # pp.pprint(aa)