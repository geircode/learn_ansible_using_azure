# Kjør som Administrator i forkant:
# Set-ExecutionPolicy RemoteSigned

# Kj�r scriptet p� UTSIDEN av roten av prosjektet. 
# F:eks. c:\bym\BackendTemplate
# cd c:\bym
# Kj�r script

$ProsjektNavnA1="Prometheus"
$ProsjektNavnA2="prometheus"
$ProsjektNavnB1="learn_ansible_using_azure"
$ProsjektNavnB2="learn_ansible_using_azure"
$PortSource="5252"
$PortTarget="5203"

# STORE BOKSTAVER. UPPER CASE ONLY.
$BrukerAdminSource="BACKENDTEMPLATE"
$BrukerAdminTarget="P360"

$ServiceIdSource="13333333-3333-3333-3333-333333333337"
$ServiceIdTarget="9979EBB2-6EEB-4DA2-8930-745404F92BE7"

$BackendTemplateAdminSource="13333333-3333-3333-3333-333333333338"
$BackendTemplateAdminTarget="ea606b81-a665-44ae-a6f5-19f78fe849b8"

  
$ProsjektServiceA1=$ProsjektNavnA1 + "Service"
$ProsjektServiceA2=$ProsjektNavnA2 + "service"
$ProsjektServiceB1=$ProsjektNavnB1 + "Service"
$ProsjektServiceB2=$ProsjektNavnB2 + "service"
 
Get-ChildItem -Filter "*" -Recurse | ForEach {  (Get-Content $_.PSPath | ForEach {$_ -creplace $ProsjektServiceA1, $ProsjektServiceB1}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $ProsjektServiceA2, $ProsjektServiceB2}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $ProsjektNavnA1, $ProsjektNavnB1}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $ProsjektNavnA2, $ProsjektNavnB2}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $PortSource, $PortTarget}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $BrukerAdminSource, $BrukerAdminTarget}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $ServiceIdSource, $ServiceIdTarget}) | Set-Content $_.PSPath }
Get-ChildItem -Filter "*" -Recurse | ForEach {  ( Get-Content $_.PSPath | ForEach {$_ -creplace $BackendTemplateAdminSource, $BackendTemplateAdminTarget}) | Set-Content $_.PSPath }
   
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $ProsjektServiceA1, $ProsjektServiceB1 }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $ProsjektNavnA1, $ProsjektNavnB1 }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $ProsjektNavnA2, $ProsjektNavnB2 }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $PortSource, $PortTarget }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $BrukerAdminSource, $BrukerAdminTarget }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $ServiceIdSource, $ServiceIdTarget }
Get-ChildItem -Filter "*" -Recurse | Rename-Item -NewName {$_.name -creplace $BackendTemplateAdminSource, $BackendTemplateAdminTarget }
