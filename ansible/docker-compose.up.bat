REM Run Dockerfile.build.bat first if you want to create your own Container Image.
cd %~dp0
docker rm -f ansible-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --remove-orphans
pause
docker exec -it ansible-1 /bin/bash