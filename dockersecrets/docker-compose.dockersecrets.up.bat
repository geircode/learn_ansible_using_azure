cd %~dp0
start cmd /C MakeDockerSecretsFolder.bat
docker rm -f dockersecrets_e911e85c-1
docker-compose -f docker-compose.dockersecrets.yml down --remove-orphans
docker-compose -f docker-compose.dockersecrets.yml up -d --build --remove-orphans
pause
docker exec -it dockersecrets_e911e85c-1 /bin/bash
